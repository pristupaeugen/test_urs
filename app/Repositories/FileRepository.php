<?php

namespace App\Repositories;

use App\Models\File;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class FileRepository
 *
 * @package App\Repositories
 */
class FileRepository extends Repository
{
    const FILE_TAG_TABLE = 'files_tags';

    /**
     * FileRepository constructor.
     *
     * @param File $model
     */
    public function __construct(File $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $fileId
     * @param int $tagId
     *
     * @return bool
     */
    public function deleteFileTag(int $fileId, int $tagId): bool
    {
        DB::table(self::FILE_TAG_TABLE)
            ->where('file_id', DB::raw(":fileId"))
            ->where('tag_id',DB::raw(":tagId"))
            ->setBindings(['fileId' => $fileId, 'tagId' => $tagId])
            ->delete();

        return true;
    }

    /**
     * @return Collection
     */
    public function getFilesWithoutTags(): Collection
    {
        $res = $this->getTable()
            ->select($this->getModel()->getTable() . '.id')
            ->addSelect(DB::raw('count(' . self::FILE_TAG_TABLE . '.file_id) AS count'))
            ->leftJoin(self::FILE_TAG_TABLE, $this->getModel()->getTable() . '.id', '=', self::FILE_TAG_TABLE . '.file_id')
            ->groupBy($this->getModel()->getTable() . '.id')
            ->having('count', '=', 0)
            ->get();


        return $res;
    }

    /**
     * @param array $ids
     * @param       $page
     * @param       $limit
     *
     * @return Collection
     */
    public function getFilesBy(array $ids, $page, $limit): Collection
    {
        return File::with('tags')
            ->select('*')
            ->whereIn('id', $ids)
            ->orderBy('name')
            ->limit($limit)
            ->offset(($page - 1) * $limit)
            ->get();
    }
}