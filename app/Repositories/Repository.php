<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class Repository
 *
 * @package App\Repositories
 */
abstract class Repository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * @param array $data
     *
     * @return Model
     */
    public function create(array $data): Model
    {
        $newInstance = $this->model->newInstance($data);
        $newInstance->save();

        return $newInstance;
    }

    /**
     * @param Model $model
     * @param       $data
     *
     * @return Model
     */
    public function update(Model $model, $data): Model
    {
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * @param Model $model
     *
     * @return bool
     * @throws \Exception
     */
    public function delete(Model $model): bool
    {
        $model->delete();
        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * @return Builder
     */
    public function getTable(): Builder
    {
        return DB::table($this->getModel()->getTable());
    }
}
