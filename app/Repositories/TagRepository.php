<?php

namespace App\Repositories;

use App\Models\Tag;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

/**
 * Class TagRepository
 *
 * @package App\Repositories
 */
class TagRepository extends Repository
{
    const FILES_TABLE      = 'files';
    const FILES_TAGS_TABLE = 'files_tags';

    /**
     * TagRepository constructor.
     *
     * @param Tag $model
     */
    public function __construct(Tag $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $names
     *
     * @return Collection
     */
    public function findTagsByNames(array $names = []): Collection
    {
        return $this->getTable()
            ->whereIn('name', $names)
            ->get();
    }

    /**
     * @param string|null $name
     *
     * @return Collection
     */
    public function findTagsByName(?string $name): Collection
    {
        $name = $name ?? '';
        $builder = $this->getTable()
            ->where('name', 'like', '%' . $name . '%');

        return $builder->get();
    }

    /**
     * @param array $includedTags
     * @param array $excludedTags
     *
     * @return Collection
     */
    public function getRelatedTags(array $includedTags, array $excludedTags): Collection
    {
        $filesByTagsBuilder = $this->getFilesByTags($includedTags, $excludedTags);
        $filesByTagsBuilder
            ->addSelect(self::FILES_TABLE . '.id as file_id');

        $fileIds = $filesByTagsBuilder->get()->pluck('file_id');
        if ($fileIds->count() === 0) {
            return new Collection();
        }

        $virtualTable = 'counts';
        $builder = $this->getTable()
            ->select('*')
            ->fromSub(
                $this->getTable()
                    ->select($this->getModel()->getTable() . '.id', $this->getModel()->getTable() . '.name')
                    ->addSelect(DB::raw('count(' . self::FILES_TAGS_TABLE . '.tag_id) AS count'))
                    ->join(self::FILES_TAGS_TABLE, $this->getModel()->getTable() . '.id', '=', self::FILES_TAGS_TABLE . '.tag_id')
                    ->groupBy($this->getModel()->getTable() . '.id')
                    ->whereIn(self::FILES_TAGS_TABLE . '.file_id', $fileIds->toArray()),
                $virtualTable)
            ->orderBy($virtualTable . '.count', 'desc')
            ->orderBy($virtualTable . '.name');

        return $builder->get();
    }

    /**
     * @param array $includedTags
     * @param array $excludedTags
     *
     * @return Collection
     */
    public function getTagsWithFiles(array $includedTags, array $excludedTags): Collection
    {
        $builder = $this->getFilesByTags($includedTags, $excludedTags);

        $builder
            ->addSelect(self::FILES_TABLE . '.id as file_id', self::FILES_TABLE . '.name as file_name', self::FILES_TABLE . '.physical_name')
            ->orderBy(self::FILES_TABLE . '.name');

        return $builder->get();
    }

    /**
     * @param array $includedTags
     * @param array $excludedTags
     *
     * @return Builder
     */
    private function getFilesByTags(array $includedTags, array $excludedTags): Builder
    {
        $builder = $this->getTable()
            ->select($this->getModel()->getTable() . '.id', $this->getModel()->getTable() . '.name')
            ->join(self::FILES_TAGS_TABLE, $this->getModel()->getTable() . '.id', '=', self::FILES_TAGS_TABLE . '.tag_id')
            ->join(self::FILES_TABLE, self::FILES_TABLE . '.id', '=', self::FILES_TAGS_TABLE . '.file_id');

        if (count($excludedTags) > 0) {

            $excludedFiles = $builder->cloneWithoutBindings([])
                ->addSelect(self::FILES_TAGS_TABLE . '.file_id')
                ->whereIn($this->getModel()->getTable() . '.name', $excludedTags)
                ->get()
                ->pluck('file_id')
                ->toArray();

            if (count($excludedFiles) > 0) {

                $builder->whereNotIn(self::FILES_TABLE.'.id', $excludedFiles);
            }
        }

        if (count($includedTags) > 0) {

            $builder->whereIn($this->getModel()->getTable() . '.name', $includedTags);
        }

        return $builder;
    }
}
