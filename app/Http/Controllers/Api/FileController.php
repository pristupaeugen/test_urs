<?php

namespace App\Http\Controllers\Api;

use App\Models\File;
use App\Models\Tag;
use App\Services\FileUploader;
use App\Services\FileManager;
use App\Http\Requests\CreateFileRequest;
use App\Http\Requests\UpdateFileRequest;

use Swagger\Annotations as SWG;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileController
 *
 * @package App\Http\Controllers\Api
 */
class FileController extends ApiController
{
    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * FileController constructor.
     *
     * @param FileUploader $fileUploader
     * @param FileManager  $fileManager
     */
    public function __construct(FileUploader $fileUploader, FileManager  $fileManager)
    {
        $this->fileUploader = $fileUploader;
        $this->fileManager  = $fileManager;
    }

    /**
     * Get file with tags
     * @param File $file
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     *
     * @SWG\Get(
     *     path="/get/{id}",
     *     description="Get file with tags",
     *     operationId="api.file.get",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     tags={"get-file"},
     *     @SWG\Parameter(
     *         name="id",
     *         description="file identification",
     *         required=true,
     *         in="path",
     *         type="number"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="File information"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     */
    public function get(File $file)
    {
        return response()->json($this->prepareFileResponse($file));
    }

    /**
     * Create file with tags
     * @param CreateFileRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/create",
     *     description="Create file with tags",
     *     operationId="api.file.create",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     tags={"create-file"},
     *     @SWG\Parameter(
     *         name="file",
     *         description="file",
     *         required=true,
     *         in="formData",
     *         type="file"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         description="file name",
     *         required=true,
     *         in="formData",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="tags",
     *         description="list of tags",
     *         required=false,
     *         in="formData",
     *         type="array",
     *         @SWG\Items(
     *                 type="string",
     *             )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="File information with list of tags"
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="The file isn't saved"
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="The file field or file name field are required"
     *     )
     * )
     */
    public function create(CreateFileRequest $request)
    {
        $filePhysicalName = $this->fileUploader->upload($request->file);
        if (is_null($filePhysicalName)) {
            abort(406, "The file isn't saved");
        }

        $file = $this->fileManager->createFileInfo([
            'name'          => $request->request->get('name'),
            'physical_name' => $filePhysicalName,
            'tags'          => ($request->request->get('tags')) ? explode(',', $request->request->get('tags')) : []
        ]);

        return response()->json($this->prepareFileResponse($file));
    }

    /**
     * Update file with tags
     * @param File $file
     * @param UpdateFileRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\POST(
     *     path="/update/{id}",
     *     description="Update file with tags",
     *     operationId="api.file.update",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     tags={"update-file"},
     *     @SWG\Parameter(
     *         name="id",
     *         description="file identification",
     *         required=true,
     *         in="path",
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="file",
     *         description="file",
     *         required=false,
     *         in="formData",
     *         type="file"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         description="file name",
     *         required=false,
     *         in="formData",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="tags",
     *         description="list of tags",
     *         required=false,
     *         in="formData",
     *         type="array",
     *         @SWG\Items(
     *                 type="string",
     *             )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="File information with list of tags"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not Found"
     *     ),
     *     @SWG\Response(
     *         response=406,
     *         description="The file isn't saved"
     *     )
     * )
     */
    public function update(File $file, UpdateFileRequest $request)
    {
        if ($request->files->count() > 0) {

            $filePhysicalName = $this->fileUploader->upload($request->file);
            if (is_null($filePhysicalName)) {
                abort(406, "The file isn't saved");
            }

            $this->fileUploader->remove($file->physical_name);
        }

        $file = $this->fileManager->updateFileInfo($file, [
            'name'          => $request->request->get('name') ?? $file->name,
            'physical_name' => $filePhysicalName ?? $file->physical_name,
            'tags'          => ($request->request->get('tags')) ? explode(',', $request->request->get('tags')) : []
        ]);

        return response()->json($this->prepareFileResponse($file));
    }

    /**
     * Delete file with tags
     * @param File $file
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     *
     * @SWG\Delete(
     *     path="/delete/{id}",
     *     description="Delete file with tags",
     *     operationId="api.file.delete",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     tags={"delete-file"},
     *     @SWG\Parameter(
     *         name="id",
     *         description="file identification",
     *         required=true,
     *         in="path",
     *         type="number"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="No content"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     */
    public function delete(File $file)
    {
        $this->fileUploader->remove($file->physical_name);
        $this->fileManager->deleteFileInfo($file);

        return response()->json(['message' => 'File was deleted'], 204);
    }

    /**
     * Delete file tag relation
     * @param File $file
     * @param Tag $tag
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     *
     * @SWG\Delete(
     *     path="/delete/{fileId}/{tagId}",
     *     description="Delete file tag relation",
     *     operationId="api.file.deleteFileTag",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     tags={"delete-file-tag"},
     *     @SWG\Parameter(
     *         name="fileId",
     *         description="file identification",
     *         required=true,
     *         in="path",
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="tagId",
     *         description="tag identification",
     *         required=true,
     *         in="path",
     *         type="number"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="No content"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not Found"
     *     )
     * )
     */
    public function deleteFileTag(File $file, Tag $tag)
    {
        $this->fileManager->deleteFileTag($file, $tag);

        return response()->json(['message' => 'File tag relation was deleted'], 204);
    }

    /**
     * Get files with tags
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     *
     * @SWG\Get(
     *     path="/search",
     *     description="Get files with tags",
     *     operationId="api.file.search",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     tags={"search-files"},
     *     @SWG\Parameter(
     *         name="search",
     *         description="search query",
     *         required=false,
     *         in="query",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         description="page number",
     *         required=false,
     *         in="query",
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         description="page limit",
     *         required=false,
     *         in="query",
     *         type="number"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Files with tags"
     *     )
     * )
     */
    public function search(Request $request)
    {
        $page  = $request->page ?? 1;
        $limit = $request->limit ?? 5;
        $search = $request->search ?? '';

        $filesData = $this->fileManager->getFilesBy($search, $page, $limit);
        if ($filesData['files']) {
            $data = [];
            foreach ($filesData['files'] as $file) {
                $data[] = $this->prepareFileResponse($file);
            }
        }

        return response()->json([
            'files'       => $data ?? [],
            'page'        => $page,
            'limit'       => $limit,
            'pages_count' => $filesData['pagesCount'],
        ]);
    }

    /**
     * @param File $file
     *
     * @return array
     */
    private function prepareFileResponse(File $file): array
    {
        return [
            'id'            => $file->id,
            'name'          => $file->name,
            'physical_name' => $file->physical_name,
            'url'           => Storage::url($file->physical_name),
            'tags'          => $file->tags()->get()->map(function (Tag $tag) {return ['id' => $tag->getKey(), 'name' => $tag->name];})
        ];
    }
}