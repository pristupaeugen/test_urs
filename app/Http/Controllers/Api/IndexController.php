<?php

namespace App\Http\Controllers\Api;

use Swagger\Annotations as SWG;

/**
 * Class IndexController
 *
 * @package App\Http\Controllers\Api
 */
class IndexController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/about",
     *     description="Returns about overview.",
     *     operationId="api.about.index",
     *     produces={"application/json"},
     *     tags={"about"},
     *     @SWG\Response(
     *         response=200,
     *         description="About overview."
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Error.",
     *     )
     * )
     */
    public function about()
    {
        return response()->json(['message' => 'about info']);
    }
}