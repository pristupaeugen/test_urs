<?php

namespace App\Http\Controllers\Api;

/**
 * Class ApiController
 *
 * @package App\Http\Controllers\Api
 *
 * @SWG\Swagger(
 *     basePath="/api/1.0",
 *     schemes={"http"},
 *     @SWG\Info(
 *         version="1.0",
 *         title="Sample filestore API",
 *     ),
 * )
 */
class ApiController extends \App\Http\Controllers\Controller
{

}