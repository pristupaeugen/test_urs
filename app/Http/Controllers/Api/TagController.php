<?php

namespace App\Http\Controllers\Api;

use App\Services\TagManager;

use Swagger\Annotations as SWG;

use Illuminate\Http\Request;

/**
 * Class TagController
 *
 * @package App\Http\Controllers\Api
 */
class TagController extends ApiController
{
    /**
     * @var TagManager
     */
    private $tagManager;

    /**
     * TagController constructor.
     *
     * @param TagManager $tagManager
     */
    public function __construct(TagManager $tagManager)
    {
        $this->tagManager = $tagManager;
    }

    /**
     * Get tags
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     *
     * @SWG\Get(
     *     path="/tags",
     *     description="Get tags by name",
     *     operationId="api.file.tags",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     tags={"get-tags"},
     *     @SWG\Parameter(
     *         name="name",
     *         description="tag name",
     *         required=false,
     *         in="query",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="tags list"
     *     )
     * )
     */
    public function tags(Request $request)
    {
        return response()->json(['tags' => $this->tagManager->getTags($request->name)]);
    }

    /**
     * Get related tags with counts
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     *
     * @SWG\Get(
     *     path="/relatedTags",
     *     description="Get related tags with counts",
     *     operationId="api.file.related_tags",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     tags={"get-related-tags"},
     *     @SWG\Parameter(
     *         name="search",
     *         description="search",
     *         required=false,
     *         in="query",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="related tags with counts"
     *     )
     * )
     */
    public function relatedTags(Request $request)
    {
        return response()->json(['tags' => $this->tagManager->getRelatedTags($request->search)]);
    }
}
