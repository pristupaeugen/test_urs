<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function files()
    {
        return $this->belongsToMany(File::class, 'files_tags', 'tag_id','file_id');
    }
}
