<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'physical_name',
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'files_tags', 'file_id', 'tag_id');
    }
}
