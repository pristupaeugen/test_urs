<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * Class FileUploader
 *
 * @package App\Services
 */
class FileUploader
{
    const STORAGE_PATH = 'public';

    /**
     * @param UploadedFile $uploadedFile
     *
     * @return null|string
     */
    public function upload(UploadedFile $uploadedFile): ?string
    {
        try {
            $uniqueName = $this->generateUniqueName($uploadedFile->getClientOriginalName());
            $uploadedFile->storeAs(self::STORAGE_PATH, $uniqueName);

            return $uniqueName;
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * @param string $fileName
     *
     * @return bool
     */
    public function remove(string $fileName): bool
    {
        $filePath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . self::STORAGE_PATH . '/' . $fileName;
        try {
            return unlink($filePath);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $name
     *
     * @return string
     */
    private function generateUniqueName($name): string
    {
        $fileExtension = substr(strrchr($name, '.'), 1);
        return md5(time() . $name) . '.' . $fileExtension;
    }
}
