<?php

namespace App\Services;

use App\Models\Tag;
use App\Repositories\TagRepository;

use Illuminate\Support\Collection;

class TagManager
{
    const REG_EXP_INCLUDED = '/\+\"([^\"]*)/';
    const REG_EXP_EXCLUDED = '/-\"([^\"]*)/';

    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * TagManager constructor.
     *
     * @param TagRepository $tagRepository
     */
    public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    /**
     * @param string $name
     *
     * @return Tag
     */
    public function createTag(string $name): Tag
    {
        return $this->tagRepository->create(['name' => $name]);
    }

    /**
     * @param array $names
     *
     * @return Collection
     */
    public function getTagsByNames(array $names): Collection
    {
        return $this->tagRepository->findTagsByNames($names);
    }

    /**
     * @param string|null $name
     *
     * @return Collection
     */
    public function getTags(?string $name): Collection
    {
        return $this->tagRepository->findTagsByName($name);
    }

    /**
     * @param string|null $search
     *
     * @return Collection
     */
    public function getRelatedTags(?string $search): Collection
    {
        $params = $this->formParams($search ?? '');
        return $this->tagRepository->getRelatedTags($params['included'], $params['excluded']);
    }

    /**
     * @param string $search
     *
     * @return Collection
     */
    public function getTagsWithFiles(string $search): Collection
    {
        $params = $this->formParams($search ?? '');
        return $this->tagRepository->getTagsWithFiles($params['included'], $params['excluded']);
    }

    /**
     * @param string $search
     *
     * @return array
     */
    private function formParams(string $search): array
    {
        $search = trim($search);
        if (empty($search)) {

            $included = [];
            foreach ($this->tagRepository->findTagsByName($search) as $tag) {
                $included[] = $tag->name;
            }

            return ['included' => $included, 'excluded' => []];
        }

        preg_match_all(self::REG_EXP_INCLUDED, $search, $names);
        $included = ($names[1]) ?? [];

        preg_match_all(self::REG_EXP_EXCLUDED, $search, $names);
        $excluded = ($names[1]) ?? [];

        foreach (array_merge($included, $excluded) as $name) {
            $search = str_replace($name, '', $search);
        }
        $search = trim(str_replace('"', '', str_replace('-', '', str_replace('+', '', $search))));

        if (!empty($search)) {
            foreach ($this->tagRepository->findTagsByName($search) as $tag) {
                $included[] = $tag->name;
            }
            $included[] = $search;
        }

        return ['included' => $included, 'excluded' => $excluded];
    }
}
