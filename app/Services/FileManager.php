<?php

namespace App\Services;

use App\Models\File;
use App\Models\Tag;
use App\Repositories\FileRepository;

use Illuminate\Support\Collection;

class FileManager
{
    /**
     * FileRepository
     */
    private $fileRepository;

    /**
     * @var TagManager
     */
    private $tagManager;

    /**
     * FileManager constructor.
     *
     * @param FileRepository $fileRepository
     * @param TagManager $tagManager
     */
    public function __construct(FileRepository $fileRepository, TagManager $tagManager)
    {
        $this->fileRepository = $fileRepository;
        $this->tagManager     = $tagManager;
    }

    /**
     * @param $data
     *
     * @return File
     */
    public function createFileInfo($data): File
    {
        $file = $this->fileRepository->create([
            'name'          => $data['name'],
            'physical_name' => $data['physical_name']]);

        $tags = $this->createTags($data['tags']);
        foreach ($tags as $tag) {
            $file->tags()->attach($tag->id);
        }

        $file->tags()->orderBy('name');

        return $file;
    }

    /**
     * @param File  $file
     * @param array $data
     *
     * @return File|\Illuminate\Database\Eloquent\Model
     */
    public function updateFileInfo(File $file, array $data)
    {
        $file = $this->fileRepository->update($file, [
            'name'          => $data['name'],
            'physical_name' => $data['physical_name']]);

        $tags = $this->createTags($data['tags']);
        foreach ($tags as $tag) {

            if ($file->tags()->get()->where('id', $tag->id)->count() === 0) {
                $file->tags()->attach($tag->id);
            }
        }

        $file->tags()->orderBy('name');

        return $file;
    }

    /**
     * @param File $file
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteFileInfo(File $file)
    {
        $this->fileRepository->delete($file);
        return true;
    }

    /**
     * @param File $file
     * @param Tag $tag
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteFileTag(File $file, Tag $tag)
    {
        $this->fileRepository->deleteFileTag($file->getKey(), $tag->getKey());
        return true;
    }

    /**
     * @param $search
     * @param $page
     * @param $limit
     *
     * @return array
     */
    public function getFilesBy($search, $page, $limit): array
    {
        $filesWithoutTags = (empty(trim($search))) ? $this->fileRepository->getFilesWithoutTags() : new Collection();
        $tagsWithFiles    = $this->tagManager->getTagsWithFiles($search);

        if ($tagsWithFiles->count() > 0 || $filesWithoutTags->count() > 0) {

            $files = $this->fileRepository->getFilesBy(array_merge($tagsWithFiles->pluck('file_id')->toArray(), $filesWithoutTags->pluck('id')->toArray()), $page, $limit);
            $count = count(array_unique(array_merge($tagsWithFiles->pluck('file_id')->unique()->toArray(), $filesWithoutTags->pluck('id')->unique()->toArray())));

            return ['files' => $files, 'pagesCount' => ceil($count/$limit)];
        }

        return ['files' => new Collection(), 'pagesCount' => 1];
    }

    /**
     * @param array $names
     *
     * @return Collection
     */
    private function createTags(array $names): Collection
    {
        $names = $this->prepareUniqueValues($names);

        $tags = $this->tagManager->getTagsByNames($names);
        foreach ($names as $name) {
            if ($tags->where('name', $name)->count() === 0) {

                $tag = $this->tagManager->createTag($name);
                $tags->put($tag->getKey(), (object)$tag->getAttributes());
            }
        }

        return $tags;
    }

    /**
     * @param array $values
     *
     * @return array
     */
    private function prepareUniqueValues(array $values): array
    {
        foreach ($values as $key => $value) {
            $values[$key] = strtolower(trim($value, '"'));
        }

        return array_unique($values);
    }
}
