import Vue from 'vue'
import Router from 'vue-router'
import Home from '../view/components/Home'
import onePage from '../view/components/OneFile'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/file/:id',
      name: 'file',
      component: onePage
    },
  ],
  mode: 'history'
})
