<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('1.0')->group(function () {
    Route::get('/about', 'Api\IndexController@about');

    Route::get('/get/{file}', 'Api\FileController@get');
    Route::post('/create', 'Api\FileController@create');
    Route::post('/update/{file}', 'Api\FileController@update');
    Route::delete('/delete/{file}', 'Api\FileController@delete');
    Route::delete('/delete/{file}/{tag}', 'Api\FileController@deleteFileTag');

    Route::get('/search', 'Api\FileController@search');

    Route::get('/tags', 'Api\TagController@tags');
    Route::get('/relatedTags', 'Api\TagController@relatedTags');
});