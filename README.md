# README #

This simple web-based application that allows users to upload, organize and search through files.

### Commands for deployment ###

Rename .env.example to .env and fill it with your credentials. Execute next commands:

* $ composer install

* $ npm install

* $ php artisan migrate

* $ php artisan storage:link

* $ php artisan swagger:generate // not required

### Documentation ###

Current web-based application has REST API on back-end and VueJs on front-end. 

Link to test site is [http://urs.lancertool.com](http://urs.lancertool.com)

Link to REST API is [http://urs.lancertool.com/swagger](http://urs.lancertool.com/swagger)